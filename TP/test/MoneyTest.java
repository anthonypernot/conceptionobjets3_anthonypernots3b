package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import exception.DeviseException;
import tp1.Money;
import tp1.MoneyList;

class MoneyTest {

	Money m1, m2;
	MoneyList b, v;
	ArrayList<Money> ml;
	
	@Before
	void deb() {
		
		ml = new ArrayList<Money>();
		
		m1 = new Money(10,"CHF");
		m2 = new Money(15, "CHF");
		
		ml.add(m1);
		ml.add(m2);
	}
	
	@Test
	void testAjouterSomme() {
	    b = new MoneyList();
		try {
			b.ajouterSomme(m1);
			b.ajouterSomme(m2);
			b.ajouterSomme(new Money(5,"CHF"));
			assertEquals(b.getList().get(0), new Money(10,"CHF"));
			
		} catch(Exception e) {System.out.println("pb dans AjouterSomme"); }
		
	}
	
	/**@Test
	public void testAdd() throws DeviseException{
		Money result = m1.add(m2);
		assertTrue(expected.equals(result));
	}*/
	
	

}
