package tp1;

import exception.DeviseException;

/**
 * classe Money permettant de generer des
 * objets permettant de cr�er une monnaie et un montant 
 * ssoci�
 * @author Cash
 *
 */
public class Money {

	/**
	 * entier indiquant un montant
	 */
	private int montant;
	/**
	 * chaine de caractere indiquant le type de monnaie
	 */
	private String devise;

	/**
	 * constructeur d'une monnaie
	 * @param mon , montant
	 * @param dev , type de monnaie
	 */
	public Money(int mon, String dev) {
		this.montant= mon;
		this.devise= dev;
	}

	/**
	 * Getteur du montant
	 * @return
	 */
	public int getMontant() { 
		return this.montant;
	}

	/**
	 * Getteur du type de monnaie
	 * @return
	 */
	public String getDevise() {
		return this.devise;
	}

	/**
	 * m�thode permettant d'ajouter de l'argent 
	 * sur le montant actuel
	 * @param m , montant � ajouter
	 * @return , montant final
	 */
	public Money add(Money m) throws DeviseException {
		return new Money(this.getMontant()+m.getMontant(), this.getDevise());
	}

	/**
	 * methode de comparaison entre le parametre 
	 * et l'objet instanci� 
	 */
	public boolean equals(Object ob) {
		
		/**if( ((Money) ob).getMontant() == this.getMontant() && ((Money) ob).getDevise() == this.getDevise()){
			return true;
		}
		return false;*/
		
		if(this == ob)
			return true;
		if(ob == null)
			return false;
		if(getClass() != ob.getClass())
			return false;
		Money other = (Money) ob;
		if(devise == null) {
			if(other.devise != null)
				return false;
		} else if( !devise.contentEquals(other.devise))
			return false;
		if (montant != other.montant)
			return false;
		return true;
		
	}
	
	/**
	 * methode retournant le montant et le type de monnaie
	 */
	public String toString() {
		return "Le montant est de "+this.getMontant()+" en "+this.getDevise()+".";
	}
	
}

