package tp1;

import java.util.ArrayList;
import java.util.List;

import exception.DeviseException;

public class MoneyList{
	
	private List<Money> list;
	
	public MoneyList() {
		list= new ArrayList<Money>();
		}
	
	public List<Money> getList() {
		return list;
		}
	
	public void ajouterSomme (Money m) throws DeviseException {
		for(int i=0; i<list.size();i++) {
			if(list.get(i)==m) {
				list.get(i).add(m);
			}else {
				list.add(m);
			}
		}
	}
	
	public String toString() {
		return "La liste : "+list;
	}
	
	 public boolean equals(Object ob) {
		for(int i=0;i<list.size();i++) {
		 if(list.get(i) == ob)
				return true;
			if(ob == null)
				return false;
			if(list.get(i).getClass() != ob.getClass())
				return false;
			Money other = (Money) ob;
			if(list.get(i).getDevise() == null) {
				if(other.getDevise() != null)
					return false;
			} else if( !list.get(i).getDevise().contentEquals(other.getDevise() ))
				return false;
			if (list.get(i).getMontant() != other.getMontant())
				return false;
		}
		return true;
	 }
	 
	 public void triMontant() {
		 
	 }
	 
	 public void triDevise() {
		 
	 }
	
}