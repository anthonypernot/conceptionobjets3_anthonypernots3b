package route;

public abstract class Vehicule {

	private double vitesse, vitesseMax;
	private String type;
	
	protected Vehicule(double v, String t) {
		this.vitesseMax=v;this.type=t;
	}
	
	public double getVitesse() {
		return this.vitesse;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void accelerer(double v) {
		if(this.vitesse>=0 && v>=0 && v<this.vitesseMax)
			this.vitesse+=v;
	}
	
	public void decelerer(double v) {
		if(this.vitesse>=0 && v>=0  && v<this.vitesseMax)
			this.vitesse-=v;
	}
	
}
