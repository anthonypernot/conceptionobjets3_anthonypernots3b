package route;

public class FabriqueIntersection implements FabriqueVehicule {

	private double probaVoiture, probaBus, probaBic, probaPieton;
	
	public FabriqueIntersection() {
		this.probaVoiture=0.8;
		this.probaPieton=0.1;
		this.probaBus=0.05;
		this.probaBic=0.05;
	}
	
	public FabriqueIntersection(double v, double b, double byc, double p) {
		
		if(v>0 && b>0 && byc>0 && p>0 && (v+b+byc+p)==1)
		this.probaVoiture=v;
		this.probaPieton=p;
		this.probaBus=b;
		this.probaBic=byc;
	}

	@Override
	public Vehicule creerVehicule() {
		return new Voiture();
	}

	public double getProbaVoiture() {
		return probaVoiture;
	}

	public double getProbaBus() {
		return probaBus;
	}


	public double getProbaBic() {
		return probaBic;
	}

	public double getProbaPieton() {
		return probaPieton;
	}
	
}
