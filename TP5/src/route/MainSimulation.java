package route;

public class MainSimulation {

	public static void main(String[] args) {
		FabriqueVehicule fvoiture = new FabriqueVoiture();
		FabriqueVehicule fpieton = new FabriquePieton();
		FabriqueIntersection fintersect = new FabriqueIntersection();
		Simulateur sim = new Simulateur(fintersect);
		sim.ecrireStats();
		FabriqueIntersection fintersect2 = new FabriqueIntersection(0.3,0.3,0.2,0.2);
		Simulateur sim2 = new Simulateur(fintersect2);
		sim2.ecrireStats();
		
		
	}
	
}
