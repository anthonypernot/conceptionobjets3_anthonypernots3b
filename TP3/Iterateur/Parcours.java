import java.util.Iterator;
import java.util.function.Consumer;

public abstract class Parcours implements Iterator<Integer> {

    protected Iterateur.TableauEntier tab;
    protected int ligneCour;
    protected int colonneCour;
    protected int nbParcourus;

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Integer next() {
        return tab.suivant();
    }

    @Override
    public void forEachRemaining(Consumer<? super Integer> action) {

    }


}
