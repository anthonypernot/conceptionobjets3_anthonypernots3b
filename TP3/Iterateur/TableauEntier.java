package Iterateur;

public class TableauEntier {

	private int[][] tab;
	
	private int larg;
	
	private int haut;
	
	public TableauEntier(int[][] t) {
		this.tab=t;
	}
	
	public int valeurA(int l, int c) {
		int resul=0;
		
		for(int i=0;i<this.tab.length;i++) {
			for(int j=0;j<this.tab.length;j++) {
				if(i==l && j==c) {
					resul=this.tab[i][j];
				}
			}
		}
		
		/**int i=0; int j=0;
			if(i==l && j==c) {
				resul = this.tab[i][j];
			}else {
				i++; j++;
			}*/
		
		return resul;
		
	}
	
	public int getLargeur() {
		return this.larg;
	}
	
	public int getHauteur() {
		return this.haut;
	}
	
	
	Parcours iterateurLigne() {
		return new ParcoursLigne();
	}
	
	Parcours iterateurZigzag() {
		return new ParcoursZigzag(this.tab);
	}
	
}
