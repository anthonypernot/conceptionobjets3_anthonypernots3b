package logo;

public abstract class DecorateurAccessoire extends Logo {

	protected Logo composant;
	protected int x, y;
	protected double prix;
	
	public DecorateurAccessoire(Logo l, String s,double p, int a, int o) {
		this.composant=l;
		this.prix=p;
		this.x=a;
		this.y=o;
		this.nomIn=s;
	}
	
	public double combienCaCoute(){
		return this.prix + composant.combienCaCoute();
	}
	
	public MyImage getLogo(){
        MyImage img = this.composant.getLogo();
        img.paintOver(nomIn, x, y);
        return img;
    }
	
}
