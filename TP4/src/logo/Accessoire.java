package logo;

public abstract class Accessoire extends Logo {

	public Accessoire() {
		
	}

	@Override
	public abstract MyImage getLogo() ;

	@Override
	public abstract double combienCaCoute() ;
	
}
