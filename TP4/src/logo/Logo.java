package logo;

public abstract class Logo {

	protected String nomIn;
	
	public MyImage getLogo() {
		return new MyImage(nomIn);
	}
	
	public abstract double combienCaCoute();
	
	public String toString() {
		return "prix : "+this.combienCaCoute();
	}
	
}
