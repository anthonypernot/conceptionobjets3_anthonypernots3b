package logo;

public class LogoLunette extends DecorateurAccessoire{

	public LogoLunette(Logo logo) {
		super(logo, "img/Sunglasses.png", 0.8, 250, 100);
	}
	
}
