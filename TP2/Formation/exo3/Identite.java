package exo3;

public class Identite {

	private String nom;
	private String prenom;
	private String nip;
	
	public Identite(String n, String p, String i) {
		this.nom=n;
		this.prenom=p;
		this.nip=i;
	}
	
	public String toString() {
		return this.nip + " : " + this.nom + " " + this.prenom; 
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}
	
}
