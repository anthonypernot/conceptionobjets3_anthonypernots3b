package exo3;

import java.util.HashMap;

public class Formation {

    private HashMap<String, Integer> notes;
    private String fID;

    public Formation(String id){
        notes = new HashMap<String, Integer>();
       fID = id;
       }

    public void ajouter(String mat, Integer coeff){
        notes.putIfAbsent(mat, coeff);
    }

    public void supprimer(String mat){
        notes.remove(mat);
    }

    public Integer coeffMat(String mat) throws Exception {
        if(notes.containsKey(mat)){
            return notes.get(mat);
        }else{
            throw(new Exception("Cette matière n'est pas dans cette formation"));
        }
    }
}