import java.util.Collection;
import java.util.HashMap;

public class Annuaire extends HashMap {
    
	private HashMap<String, String> tab;
	
    public Annuaire(){
       tab = new HashMap<String, String>();
    }

    public void domaine(){
        System.out.println(tab.values());
    }

    public String acces(String c){
    	return tab.get(c);
    }

    public void adjonction(String c, String v){
        
    	int res = 0;
        for (Object cle : this.keySet()) {
            if(cle == c)
                res+=1;
        }
        if (res == 0) {
            tab.put(c, v);
        }
    }

    public void suppression(String c){
     
    	int res = 0;
        for (Object cle : this.keySet()) {
            if(cle.equals(c))
                res+=1;
        }
        if (res == 0){
            this.remove(c);
        }
    }

    public void changment(String c, String v){

        int res = 0;
        for (Object cle : this.keySet()) {
            if(cle.equals(c))
                res+=1;
        }
        if (res == 0){
            this.suppression(c);
            this.adjonction(c, v);
        }
    }
}