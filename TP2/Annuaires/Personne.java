

public class Personne {

	private String nom;
	
	private String prenom;
	
	private String num;
	
	public Personne(String n,String p,String t) {
		this.nom=n;
		this.prenom=p;
		this.num=t;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	
	
	
}
