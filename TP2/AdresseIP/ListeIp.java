import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class ListeIp {

	private Set<AdresseIP> ips;
	
	public ListeIp(boolean bool) {
		this.ips= new HashSet<AdresseIP>();
		if(bool) {
			this.ips = new TreeSet<AdresseIP>();
		}
	}
	
	public void chargerFichier(String name) throws IOException {
		String ligne;
		BufferedReader fichier = new BufferedReader(new FileReader(name));
		while ((ligne = fichier.readLine()) != null) {
			String split[] = ligne.split(" ");
			this.ips.add(new AdresseIP(split[0], ligne));
		}
		fichier.close();
	}
	
	public String toString() {
		
		return this.ips.toString();
		
		/**
	    for(int i=0; i < this.ips.size(); i++) {
			System.out.println(this.ips.get(i));
		}
		*/
	}
	
}
