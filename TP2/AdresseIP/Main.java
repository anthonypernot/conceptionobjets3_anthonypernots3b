import java.io.IOException;

public class Main {

	public static ListeIp lAdresse;
	
	public static void main(String args[]) throws IOException{
		/**
		 * test de la liste non tri�e (pas de TreeSet)
		 */
		lAdresse = new ListeIp(false);
		lAdresse.chargerFichier("logs.txt");
		System.out.println(lAdresse);
		
		/**
		 * test de la liste tri�e (pas de HashSet)
		 */
		lAdresse = new ListeIp(true);
		lAdresse.chargerFichier("logs.txt");
		System.out.println(lAdresse);
		
	}
	
}
