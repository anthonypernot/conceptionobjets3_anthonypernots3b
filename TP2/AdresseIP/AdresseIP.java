
public class AdresseIP {

	private String ip;
	private String descriptif;
	
	public AdresseIP(String i, String d) {
		this.ip=i;
		this.descriptif=d;
	}
	
	public int compareTo(AdresseIP IPrandom) {
		
		int flag = 0;
		if (!this.ip.equals(IPrandom.getIp())) {
			String partIP[] = this.ip.split("\\.");
			String partOtherIP[] = IPrandom.getIp().split("\\.");
			for (int i = 0; i < 5; i++) {
				if (Integer.valueOf(partIP[i]) < Integer.valueOf(partOtherIP[i])) {
					flag = -1;
					break;
				} else if (Integer.valueOf(partIP[i]) > Integer.valueOf(partOtherIP[i])) {
					flag = 1;
					break;
				}
			}
		}
	    return flag;
	}
	
	private String getIp() {
		
		return this.ip;
	}
	
	@SuppressWarnings("unused")
	private String getDescriptif() {
		return this.descriptif;
	}

	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		AdresseIP other = (AdresseIP) obj;
		if(ip == null) {
			if(other.ip != null)
				return false;
		}else if(!ip.contentEquals(other.ip))
			return false;
		return true;
	}
	
	public String toString() {
		return this.ip + " " + this.descriptif;
	}
	
	
}
